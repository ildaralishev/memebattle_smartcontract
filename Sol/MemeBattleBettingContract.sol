// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title BettingContract
 * @dev A smart contract for placing bets using ERC20 tokens.
 */
contract BettingContract is Ownable {

    // address for referral and developers 1% commission
    address private addr1 = 0x2A27568a8a13E8b1892Ad72Ae4cd653Df9A4E1D2;
    // address for referral and developers 1% commission
    address private addr2 = 0x865612B3F44d94F4eC890Ee4a0A99dAccf608429;
    // address for referral and developers 1% commission
    address private addr3 = 0xdbc25CC7551D582ae127934C66f2855112672329;
    // address for referral and developers 3% commission
    address private addr4 = 0xc9d140bCcbbF71C74b3b7c85AA8849D34c76eDe8;

    struct UserBet {
        uint256 betAmount;
        bool exists;
        bool isPlayed;
    }

    mapping(address => UserBet) public userBets;

    IERC20 private usdtToken;

    /**
     * @dev Constructor to initialize the BettingContract.
     * @param _usdtTokenAddress The address of the ERC20 token to be used for betting.
     */
    constructor(address _usdtTokenAddress) {
        usdtToken = IERC20(_usdtTokenAddress);
    }

    function _transferFromTokens(address _from, address _to, uint256 _amount) internal {
        SafeERC20.safeTransferFrom(usdtToken, _from, _to, _amount);
    }
    
    /**
     * @dev Function to allow users to place a bet by sending ERC20 tokens to the contract.
     * @param betAmount The amount of ERC20 tokens the user wants to bet.
     */
    function placeBet(uint256 betAmount) external {
        require(!userBets[msg.sender].exists, "Bet already placed");

        _transferFromTokens(msg.sender, address(this), betAmount);
        
        userBets[msg.sender] = UserBet(betAmount, true, false);
    }

    /**
     * @dev Function to check the status of a user's bet transaction.
     * @param userAddress The address of the user whose bet status needs to be checked.
     * @return A string representing the status of the user's bet transaction. Possible values are:
     *   - "Do not exist": The user has not placed a bet.
     *   - "Finished": The user has played the game and the bet transaction is finished.
     *   - "In progress": The user has placed a bet, but the bet transaction is still in progress.
     *   - "Success": The user has successfully placed a bet and the bet transaction is complete.
     */
    function checkBetStatus(address userAddress) external view returns (string memory) {
        UserBet memory userBet = userBets[userAddress];
        
        if (!userBet.exists) {
            return "Do not exist";
        } else if (userBet.isPlayed) {
            return "Finished";
        } else if (userBet.betAmount > 0) {
            return "In progress";
        } else {
            return "Success";
        }
    }

    /**
     * @dev Finish the game and transfer winnings to the winner.
     * Can only be called by the contract owner.
     * @param winnerAddress The address of the winner.
     * @param loserAddress The address of the loser.
     */
    function finishGame(
            address winnerAddress, 
            address loserAddress, 
            address referralAddresses
        ) external onlyOwner {

        UserBet storage winnerBet = userBets[winnerAddress];
        UserBet storage loserBet = userBets[loserAddress];

        winnerBet.isPlayed = true;
        winnerBet.exists = false;

        loserBet.isPlayed = true;
        loserBet.exists = false;

        uint256 prizeAmount = (winnerBet.betAmount * 93) / 100;
        uint256 commissionReferral = (winnerBet.betAmount * 1) / 100;
        uint256 commissionAddr1 = (winnerBet.betAmount * 1) / 100;
        uint256 commissionAddr2 = (winnerBet.betAmount * 1) / 100;
        uint256 commissionAddr3 = (winnerBet.betAmount * 1) / 100;
        uint256 commissionAddr4 = (winnerBet.betAmount * 3) / 100;

        SafeERC20.safeTransfer(usdtToken, winnerAddress, prizeAmount);
        SafeERC20.safeTransfer(usdtToken, referralAddresses, commissionReferral);
        SafeERC20.safeTransfer(usdtToken, addr1, commissionAddr1);
        SafeERC20.safeTransfer(usdtToken, addr2, commissionAddr2);
        SafeERC20.safeTransfer(usdtToken, addr3, commissionAddr3);
        SafeERC20.safeTransfer(usdtToken, addr4, commissionAddr4);

        winnerBet.betAmount = 0;
        loserBet.betAmount = 0;

    }

    /**
     * @notice Allow users to make in-game purchases with a distribution of funds.
     * @param purchaseAmount The amount of USDT tokens.
     * @param referralAddress The address of the referral.
     */
    function makeInGamePurchase(uint256 purchaseAmount, address referralAddress) external {
        require(purchaseAmount > 0, "Purchase amount must be greater than 0");
        
        uint256 addr1Amount = (purchaseAmount * 17) / 100;
        uint256 addr2Amount = (purchaseAmount * 17) / 100;
        uint256 addr3Amount = (purchaseAmount * 16) / 100;
        uint256 addr4Amount = (purchaseAmount * 49) / 100;
        uint256 referralAmount = (purchaseAmount * 1) / 100;
        
        _transferFromTokens(msg.sender, referralAddress, referralAmount);
        _transferFromTokens(msg.sender, addr1, addr1Amount);
        _transferFromTokens(msg.sender, addr2, addr2Amount);
        _transferFromTokens(msg.sender, addr3, addr3Amount);
        _transferFromTokens(msg.sender, addr4, addr4Amount);
    }


    /**
     * @dev Function to allow the owner of the contract to withdraw available funds from the contract.
     * @param amount The amount of ERC20 tokens to be withdrawn from the contract.
     * Only the contract owner can call this function.
     */
    function withdrawFunds(uint256 amount) external onlyOwner {
        uint256 contractBalance = usdtToken.balanceOf(address(this));
        require(amount <= contractBalance, "Insufficient contract balance");
        SafeERC20.safeTransfer(usdtToken, owner(), amount);
    }
}
